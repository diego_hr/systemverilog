module ram ( inout wire [7:0] data,
	     input logic [3:0] address,
	     input logic n_cs, n_we, n_oe );

   logic [7:0] 		 mem [0:15];


   assign data = (~n_cs & ~n_oe ) ? mem[address] : 'z;

   always_latch
     if ( ~n_cs & ~n_we & n_oe )
       mem[address] <= data;
endmodule // ram