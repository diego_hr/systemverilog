`timescale 1ns/1ps

module sipo #( parameter N=8 ) ( input logic clk, reset, din,
				 output logic [N-1:0] dout );
   
   logic [N-1:0] 				   temp;

   always_ff @( posedge clk, posedge reset )
     begin
	if( reset )
	  temp<='0;
	else
	  temp<={temp[N-2:0], din};
     end
   always_comb
     dout=temp;
 
endmodule // top