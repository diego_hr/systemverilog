`timescale 1ns/1ps

module sipo_tb;
   logic reset;
   logic clk;
   logic din;
   logic [7:0] dout;


   initial begin
      #10 clk=1;
      #10 reset='0;
      #10 reset='1;
      #10 reset='0;
      #10 din='0;
   end

   always begin
      #10 clk=~clk;
      din=clk;
   end // always begin

   sipo #(.N(8)) uuv (.*);
endmodule // sipo_tb
