`timescale 1ns/1ps

module top( input logic clk, reset, load,
	    input logic [7:0] din_par,
	    output logic [7:0] dout_par );

   logic 		       ser;

   piso piso0 (.*, .dout(ser), .din(din_par));
   sipo sipo0 (.*, .din(ser), .dout(dout_par));
   
endmodule // top