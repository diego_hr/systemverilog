gui_state_default_create -off -ini

# Globals
gui_set_state_value -category Globals -key open_wave_inside -value true

# Layout
gui_set_state_value -category Layout -key child_console_size_x -value 653
gui_set_state_value -category Layout -key child_console_size_y -value 161
gui_set_state_value -category Layout -key child_data_coltype -value 90
gui_set_state_value -category Layout -key child_data_colvalue -value 57
gui_set_state_value -category Layout -key child_data_colvariable -value 114
gui_set_state_value -category Layout -key child_data_size_x -value 226
gui_set_state_value -category Layout -key child_data_size_y -value 300
gui_set_state_value -category Layout -key child_hier_col3 -value {-1}
gui_set_state_value -category Layout -key child_hier_colpd -value 0
gui_set_state_value -category Layout -key child_hier_size_x -value 109
gui_set_state_value -category Layout -key child_hier_size_y -value 300
gui_set_state_value -category Layout -key child_schematic_docknewline -value false
gui_set_state_value -category Layout -key child_schematic_pos_x -value {-2}
gui_set_state_value -category Layout -key child_schematic_pos_y -value {-15}
gui_set_state_value -category Layout -key child_schematic_size_x -value 911
gui_set_state_value -category Layout -key child_schematic_size_y -value 580
gui_set_state_value -category Layout -key child_source_docknewline -value false
gui_set_state_value -category Layout -key child_source_pos_x -value {-2}
gui_set_state_value -category Layout -key child_source_pos_y -value {-15}
gui_set_state_value -category Layout -key child_source_size_x -value 947
gui_set_state_value -category Layout -key child_source_size_y -value 562
gui_set_state_value -category Layout -key child_wave_colname -value 64
gui_set_state_value -category Layout -key child_wave_colvalue -value 20
gui_set_state_value -category Layout -key child_wave_docknewline -value false
gui_set_state_value -category Layout -key child_wave_left -value 88
gui_set_state_value -category Layout -key child_wave_pos_x -value {-2}
gui_set_state_value -category Layout -key child_wave_pos_y -value {-15}
gui_set_state_value -category Layout -key child_wave_right -value 223
gui_set_state_value -category Layout -key child_wave_size_x -value 321
gui_set_state_value -category Layout -key child_wave_size_y -value 295
gui_set_state_value -category Layout -key main_pos_x -value 137
gui_set_state_value -category Layout -key main_pos_y -value 265
gui_set_state_value -category Layout -key main_size_x -value 790
gui_set_state_value -category Layout -key main_size_y -value 908
gui_set_state_value -category Layout -key stand_wave_child_docknewline -value false
gui_set_state_value -category Layout -key stand_wave_child_dockstate -value bottom
gui_set_state_value -category Layout -key stand_wave_child_size_x -value 852
gui_set_state_value -category Layout -key stand_wave_child_size_y -value 199

# list_value_column

# Sim

# Assertion

# Stream

# Data

# TBGUI

# Driver

# Class

# Member

# ObjectBrowser

# UVM

# Local

# Backtrace

# Exclusion
gui_set_state_value -category Exclusion -key favorite_exclude_annotation -value {}

# SaveSession

# FindDialog
gui_create_state_key -category FindDialog -key m_pMatchCase -value_type bool -value false
gui_create_state_key -category FindDialog -key m_pMatchWord -value_type bool -value false
gui_create_state_key -category FindDialog -key m_pUseCombo -value_type string -value {}
gui_create_state_key -category FindDialog -key m_pWrapAround -value_type bool -value true

# Widget_History
gui_create_state_key -category Widget_History -key {dlgSimSetup|m_setupTab|tab pages|SimTab|m_VPDCombo} -value_type string -value inter.vpd
gui_create_state_key -category Widget_History -key {dlgSimSetup|m_setupTab|tab pages|SimTab|m_curDirCombo} -value_type string -value {/home/diego/Documents/Synopsys/SystemVerilog/mux /home/diego/Documents/Synopsys/SystemVerilog/mux/first_rev/vcs_presynth /home/diego/Documents/Synopsys/Verilog/counter /home/diego/EDA/VCS/gui/dve/bin}
gui_create_state_key -category Widget_History -key {dlgSimSetup|m_setupTab|tab pages|SimTab|m_exeCombo} -value_type string -value {./simv simv /home/diego/EDA/VCS/bin/vcs}

# SearchDialog
gui_create_state_key -category SearchDialog -key MatchCase -value_type bool -value false
gui_create_state_key -category SearchDialog -key MatchWord -value_type bool -value true
gui_create_state_key -category SearchDialog -key SearchMode -value_type string -value Wildcards
gui_create_state_key -category SearchDialog -key UseCombo -value_type bool -value true


gui_state_default_create -off
