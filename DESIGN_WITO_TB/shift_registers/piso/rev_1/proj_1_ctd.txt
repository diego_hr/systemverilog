

End Points:

   Index   Instance     Pin   Type   Start Clock   End Clock    Clock Delay   Req Per   Slack
   -----   --------     ---   ----   -----------   ---------    -----------   -------   -----
       1   un1_clk[0]   D     reg    System:f      piso|clk:r         3.245     1.000   0.274
       2   un1_clk[1]   D     reg    System:f      piso|clk:r         3.245     1.000   0.274
       3   un1_clk[2]   D     reg    System:f      piso|clk:r         3.245     1.000   0.274
       4   un1_clk[3]   D     reg    System:f      piso|clk:r         3.245     1.000   0.274
       5   un1_clk[4]   D     reg    System:f      piso|clk:r         3.245     1.000   0.274
       6   un1_clk[5]   D     reg    System:f      piso|clk:r         3.245     1.000   0.274
       7   un1_clk[6]   D     reg    System:f      piso|clk:r         3.245     1.000   0.274
       8   un1_clk[0]   D     reg    piso|clk:r    piso|clk:r         3.245     1.000   0.354
       9   un1_clk[1]   D     reg    piso|clk:r    piso|clk:r         3.245     1.000   0.354
      10   un1_clk[2]   D     reg    piso|clk:r    piso|clk:r         3.245     1.000   0.354
      11   un1_clk[3]   D     reg    piso|clk:r    piso|clk:r         3.245     1.000   0.354
      12   un1_clk[4]   D     reg    piso|clk:r    piso|clk:r         3.245     1.000   0.354
      13   un1_clk[5]   D     reg    piso|clk:r    piso|clk:r         3.245     1.000   0.354
      14   un1_clk[6]   D     reg    piso|clk:r    piso|clk:r         3.245     1.000   0.354


Start Points:

   Index   Instance     Pin   Type   Start Clock   Clock Delay   Slack
   -----   --------     ---   ----   -----------   -----------   -----
       1   un1_clk_27   Q     reg    System:f            0.000   0.274
       2   un1_clk_23   Q     reg    System:f            0.000   0.274
       3   un1_clk_19   Q     reg    System:f            0.000   0.274
       4   un1_clk_15   Q     reg    System:f            0.000   0.274
       5   un1_clk_11   Q     reg    System:f            0.000   0.274
       6   un1_clk_7    Q     reg    System:f            0.000   0.274
       7   un1_clk_3    Q     reg    System:f            0.000   0.274
       8   un1_clk[1]   Q     reg    piso|clk:r          3.245   0.354
       9   un1_clk[2]   Q     reg    piso|clk:r          3.245   0.354
      10   un1_clk[3]   Q     reg    piso|clk:r          3.245   0.354
      11   un1_clk[4]   Q     reg    piso|clk:r          3.245   0.354
      12   un1_clk[5]   Q     reg    piso|clk:r          3.245   0.354
      13   un1_clk[6]   Q     reg    piso|clk:r          3.245   0.354
      14   un1_clk[7]   Q     reg    piso|clk:r          3.245   0.354
