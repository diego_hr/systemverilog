`timescale 1ns/1ps

module piso_tb;
   logic clk;
   logic reset;
   logic [7:0] din;
   logic       dout;
   logic       load;
   

   initial begin
      #0 clk=1;
      #10 reset=0;
      #10 reset=1;
      #10 reset=0;
      #10 load=0;
      #100 load=1;
      #100 load=0;
   end

   always begin
      #10 clk=!clk;
      din='b10101010;
   end

   piso #(.N(8))  uuv (.*);
endmodule // piso_tb
