`timescale 1ns/1ps

module piso_tb;
   logic clk;
   logic reset;
   logic [7:0] din;
   logic       dout;


   initial begin
      #10 clk=0;
      #10 reset='0;
      #10 reset='1;
      #100 reset='0;
      #10 din='b11110000;
   end

   always begin
      #10 clk=~clk;
      #10 din='b11110000;
   end

   piso uut (.*);

   initial #300000 $finish;
   
endmodule // piso_tb
