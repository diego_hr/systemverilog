`timescale 1ns/1ps

module top_tb;
   logic clk;
   logic reset;
   logic load;
   logic [7:0] din_par;
   logic [7:0] dout_par;

   initial begin
      #10 clk=0;
      #10 reset=0;
      #20 reset=1;
      #10 reset=0;
      #10 din_par='0;
      #10 load='0;
      #10 load='1;
      #10 load='0;
   end

   always begin
      #10 clk=!clk;
      #10 din_par='b10101010;
   end

   top uuv (.*);
endmodule // top_tb

      

   