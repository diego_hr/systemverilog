module add_sub #( paremeter WIDTH=4 )
   ( input logic [WIDTH-1:0] input_a, input_b,
     input logic cin,
     input logic control,
     output logic [(WIDTH*2)-1:0] output_b,
     output logic cout
   );


   