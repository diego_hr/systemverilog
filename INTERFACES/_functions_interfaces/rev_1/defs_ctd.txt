

End Points:

   Index   Instance   Pin   Type   Start Clock         End Clock           Clock Delay   Req Per    Slack
   -----   --------   ---   ----   -----------         ---------           -----------   -------    -----
       1   c[7]       D     reg    counter|bus_clk:r   counter|bus_clk:r         3.245     1.000   -0.133
       2   c[6]       D     reg    counter|bus_clk:r   counter|bus_clk:r         3.245     1.000   -0.119
       3   c[5]       D     reg    counter|bus_clk:r   counter|bus_clk:r         3.245     1.000   -0.106
       4   c[4]       D     reg    counter|bus_clk:r   counter|bus_clk:r         3.245     1.000   -0.092
       5   c[3]       D     reg    counter|bus_clk:r   counter|bus_clk:r         3.245     1.000   -0.079
       6   c[2]       D     reg    counter|bus_clk:r   counter|bus_clk:r         3.245     1.000   -0.065
       7   c[1]       D     reg    counter|bus_clk:r   counter|bus_clk:r         3.245     1.000    0.213
       8   c[0]       D     reg    counter|bus_clk:r   counter|bus_clk:r         3.245     1.000    0.282


Start Points:

   Index   Instance   Pin   Type   Start Clock         Clock Delay    Slack
   -----   --------   ---   ----   -----------         -----------    -----
       1   c[0]       Q     reg    counter|bus_clk:r         3.245   -0.133
       2   c[0]       Q     reg    counter|bus_clk:r         3.245   -0.119
       3   c[0]       Q     reg    counter|bus_clk:r         3.245   -0.106
       4   c[0]       Q     reg    counter|bus_clk:r         3.245   -0.092
       5   c[0]       Q     reg    counter|bus_clk:r         3.245   -0.079
       6   c[0]       Q     reg    counter|bus_clk:r         3.245   -0.065
       7   c[0]       Q     reg    counter|bus_clk:r         3.245    0.213
       8   c[0]       Q     reg    counter|bus_clk:r         3.245    0.282
