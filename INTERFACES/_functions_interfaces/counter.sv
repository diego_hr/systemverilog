`include "defs.sv"

import defs::*;

interface main_bus( input logic clk );
   logic 	       reset;
   data                data_out;

   modport out ( input clk,
		 input reset,
		 output data_out );
endinterface // main_bus 


module counter( main_bus.out bus );
   uint_8 c;
   
   always_ff @( posedge bus.clk, posedge bus.reset )
     begin
	if( bus.reset )
	  c='0;
	else
	  c++;
     end

   always_comb
     bus.data_out=c;
endmodule // counter




