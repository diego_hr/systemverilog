package defs;
	
   typedef  logic [6:0] input_0;
   
   function automatic [13:0] ads( input [13:0] data_a, data_b );   
      return data_a + data_b;
   endfunction // ads

   function automatic [13:0] sus( input [13:0] data_a, data_b );
      return data_a - data_b;
   endfunction // sus

endpackage // defs
   