`include "defs.sv"

import defs::*;
module add_sub ( input input_0 a, b,
		 output logic [13:0] c,
		 input logic sel );

	always_comb
	begin
		if( sel )
			c=defs::ads(a, b);
		else
			c=defs::sus(a, b);
	end
   //assign c= sel ? defs::ads(a, b) : defs::sus(a, b);
endmodule // add_sub