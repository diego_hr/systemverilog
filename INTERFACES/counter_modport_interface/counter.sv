interface counter_bus ( input logic clk ); //Interface declaration
	logic reset;
	//logic en;
	logic up_down;
	logic [7:0] qdata;

modport cin  (input reset, up_down, clk,
	          ref qdata);
   
endinterface: counter_bus

module counter ( counter_bus.cin bus );

always @( posedge bus.clk ) begin
	if(bus.reset)
		bus.qdata<='0;
	else unique case (bus.up_down)
		'0: bus.qdata<=bus.qdata+1;
		'1: bus.qdata<=bus.qdata-1;
	endcase
end	
endmodule

		