/*
 *
 * 
 */   
module counter_tb;
   logic clk;

   /* Connect the bus and UUT */
   counter_bus bus0 (.clk(clk));
   counter u0 (.bus(bus0));

   initial begin
      #10 $display("@%t All ok", $time);
      #20 clk=0;
      #10 bus0.reset=1;
      #10 bus0.reset=0;
      #10 bus0.up_down=0;
   end
   
   always begin
      #20 clk=~clk;
      $display("Q=%d reset=%b up_down=%b", bus0.qdata, bus0.reset, bus0.up_down);
      if(bus0.qdata==255) $finish;
   end
endmodule // counter_tb

   