`include "definitions.sv"

module alu ( input definitions::instruction_t IW,
	         input logic clk,
	         output logic [31:0] result );

   always_ff @( posedge clk )
     begin
	case( IW.opcode )
	  definitions::ADD: result = IW.a + IW.b;
	  definitions::SUB: result = IW.a - IW.b;
	  definitions::MUL: result = definitions::multiplier(IW.a, IW.b );
	endcase // case ( IW.opcode )
     end
endmodule // alu