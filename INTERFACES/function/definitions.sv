package definitions;

   parameter VERSION="1.1";
   
   typedef enum{ ADD, SUB, MUL } opcodes_t;
   
   typedef struct {
     logic [31:0] a, b;
     opcodes_t opcode;
   } instruction_t;

function automatic [31:0] multiplier( input [31:0] a, b );
   return a*b;
endfunction // multiplier

endpackage // definitions
