
# 1000 : define_clock [get_ports {clk}] -name {alu|clk} -ref_rise {0.000000} -ref_fall {500.000000} -uncertainty {0.000000} -period {1000.000000} -clockgroup {Autoconstr_clkgroup_0} -rise {0.000000} -fall {500.000000} 

create_clock  -name {alu|clk} [get_ports {clk}]  -period {1000.000} -waveform {0.000 500.000}

set_property IOB true [get_cells {result[3]}]
set_property IOB true [get_cells {result[2]}]
set_property IOB true [get_cells {result[1]}]
set_property IOB true [get_cells {result[0]}]
set_property IOB true [get_cells {result[18]}]
set_property IOB true [get_cells {result[17]}]
set_property IOB true [get_cells {result[16]}]
set_property IOB true [get_cells {result[15]}]
set_property IOB true [get_cells {result[14]}]
set_property IOB true [get_cells {result[13]}]
set_property IOB true [get_cells {result[12]}]
set_property IOB true [get_cells {result[11]}]
set_property IOB true [get_cells {result[10]}]
set_property IOB true [get_cells {result[9]}]
set_property IOB true [get_cells {result[8]}]
set_property IOB true [get_cells {result[7]}]
set_property IOB true [get_cells {result[6]}]
set_property IOB true [get_cells {result[5]}]
set_property IOB true [get_cells {result[4]}]
set_property IOB true [get_cells {result[31]}]
set_property IOB true [get_cells {result[30]}]
set_property IOB true [get_cells {result[29]}]
set_property IOB true [get_cells {result[28]}]
set_property IOB true [get_cells {result[27]}]
set_property IOB true [get_cells {result[26]}]
set_property IOB true [get_cells {result[25]}]
set_property IOB true [get_cells {result[24]}]
set_property IOB true [get_cells {result[23]}]
set_property IOB true [get_cells {result[22]}]
set_property IOB true [get_cells {result[21]}]
set_property IOB true [get_cells {result[20]}]
set_property IOB true [get_cells {result[19]}]

#Constraints which are not forward annotated in XDC and intentionally commented out (unused and unsupported constraints)

#Synplify Premier Physical Plus Generated Physical Constraints
#User specified region constraints
