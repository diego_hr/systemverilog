
# 1000 : define_clock [get_ports {clk}] -name {counter|clk} -ref_rise {0.0} -ref_fall {0.500000} -uncertainty {0.000000} -period {1.000000} -clockgroup {Autoconstr_clkgroup_0} -rise {0.0} -fall {0.500000} 

create_clock  -name {counter|clk} [get_ports {clk}]  -period {   1.000} -waveform {0.000 0.500}


#Constraints which are not forward annotated in XDC and intentionally commented out (unused and unsupported constraints)

#Synplify Premier Physical Plus Generated Physical Constraints
#User specified region constraints
