`timescale 1ns/1ps

`define big typedef int c
`define lit typedef byte c

module counter #( parameter n=32 )
   ( input logic clk, reset,
     output logic [n-1:0] q );

`big;

   always_ff @( posedge clk, posedge reset )
     begin: seq
	if( reset )
	  c=0;
	else
	  c++;
     end

   always_comb
     q=c;
endmodule // counter