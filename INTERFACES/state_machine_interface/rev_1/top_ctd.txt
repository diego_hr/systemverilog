

End Points:

   Index   Instance          Pin   Type   Start Clock        End Clock          Clock Delay   Req Per    Slack
   -----   --------          ---   ----   -----------        ---------          -----------   -------    -----
       1   busout\.data[1]   D     reg    top|busout_clk:r   top|busout_clk:r         2.932     1.000   -0.064
       2   busout\.data[2]   D     reg    top|busout_clk:r   top|busout_clk:r         2.932     1.000   -0.064
       3   busout\.data[3]   D     reg    top|busout_clk:r   top|busout_clk:r         2.932     1.000   -0.064
       4   current[1]        D     reg    top|busout_clk:r   top|busout_clk:r         2.932     1.000   -0.064
       5   current[0]        D     reg    top|busout_clk:r   top|busout_clk:r         2.932     1.000   -0.004


Start Points:

   Index   Instance     Pin   Type   Start Clock        Clock Delay    Slack
   -----   --------     ---   ----   -----------        -----------    -----
       1   current[0]   Q     reg    top|busout_clk:r         2.932   -0.064
       2   current[0]   Q     reg    top|busout_clk:r         2.932   -0.064
       3   current[0]   Q     reg    top|busout_clk:r         2.932   -0.064
       4   current[0]   Q     reg    top|busout_clk:r         2.932   -0.064
       5   current[0]   Q     reg    top|busout_clk:r         2.932   -0.004
