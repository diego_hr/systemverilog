`timescale 1ns/1ps

module top_tb;
   logic clk;

   bus mainbus(.clk(clk));
   top uuv(.busout(mainbus));

   initial begin
      //#0mainbus.clk=1;
      #0mainbus.reset=0;
      #10mainbus.reset=1;
      #10mainbus.reset=0;
      $display( "reset task ok" );
   end

   always begin
      #10 mainbus.clk=!mainbus.clk;
   end

   initial #50000000 $finish;
endmodule // top_tb
