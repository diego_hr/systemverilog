

End Points:

   Index   Instance          Pin   Type   Start Clock        End Clock          Clock Delay   Req Per    Slack
   -----   --------          ---   ----   -----------        ---------          -----------   -------    -----
       1   busout\.data[1]   D     reg    top|busout_clk:r   top|busout_clk:r         4.406     1.000   -0.497
       2   busout\.data[2]   D     reg    top|busout_clk:r   top|busout_clk:r         4.406     1.000   -0.497
       3   busout\.data[3]   D     reg    top|busout_clk:r   top|busout_clk:r         4.406     1.000   -0.497
       4   busout\.data[4]   D     reg    top|busout_clk:r   top|busout_clk:r         4.406     1.000   -0.497
       5   current[1]        D     reg    top|busout_clk:r   top|busout_clk:r         4.406     1.000   -0.497
       6   current[0]        D     reg    top|busout_clk:r   top|busout_clk:r         4.406     1.000   -0.398


Start Points:

   Index   Instance     Pin   Type   Start Clock        Clock Delay    Slack
   -----   --------     ---   ----   -----------        -----------    -----
       1   current[0]   Q     reg    top|busout_clk:r         4.406   -0.497
       2   current[0]   Q     reg    top|busout_clk:r         4.406   -0.497
       3   current[0]   Q     reg    top|busout_clk:r         4.406   -0.497
       4   current[0]   Q     reg    top|busout_clk:r         4.406   -0.497
       5   current[0]   Q     reg    top|busout_clk:r         4.406   -0.497
       6   current[0]   Q     reg    top|busout_clk:r         4.406   -0.398
