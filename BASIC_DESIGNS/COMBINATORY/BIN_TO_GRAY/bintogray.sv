/* Bin to Gray
 * encoder.
 * 3 bits.
 */

`timescale 1ns/1ps

module bintogray ( output wire [2:0] g,
	      input wire [2:0] b );


   xor(g[0], b[1], b[0]);
   xor(g[1], b[2], b[1]);

   assign g[2]=b[2];

endmodule // gtob
