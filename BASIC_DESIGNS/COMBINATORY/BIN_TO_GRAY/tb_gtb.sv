`timescale 1ns/1ps

module tb_bintogray;

   reg [2:0] g;
   reg [2:0] b;


   initial begin
      $display("Initialize with 0's\n");
      #2 b='0;
      $display("Done!\n");
   end

   always begin
      #2 b='b001;
      #2 b='b010;
      #2 b='b011;
      #2 b='b100;
      #2 b='b101;
      #2 b='b110;
      #2 b='b111;
      $monitor($time,,"bin=%b",b,,,"gray=%b",g);
   end

   initial #30 $finish;

   bintogray UUD (.*);

endmodule // tb_gtob
