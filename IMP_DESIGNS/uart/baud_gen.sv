module baud_gen ( output logic baud_clk,
		  input logic reset,
		  input logic clk
		);
   
   logic [6:0] 		      tickgen;

   always@ ( posedge clk, posedge reset )
     begin
	if ( reset )
	  begin
	     tickgen<='0;
	     baud_clk<='0;
	  end
	else if( tickgen == 27 )
	  begin
	     baud_clk<=!baud_clk;
	     tickgen<='0;
	  end
	else
	  tickgen<=tickgen+'1;
     end // always@ ( posedge clk )
endmodule // baud_gen		    
		    
		    

	