`timescale 1ns/1ps

module top_tb;
   logic clk;
   logic reset;
   logic send;
   logic [7:0] data;
   logic       rdy;
   logic       busy;
   logic       tx;


   initial begin
      #10 clk=0;
      #10 reset=1;
      #10 reset=1;
      #10 reset=1;
      #10 reset=0;
      #0 send=0;
   end

   always begin
      #10 clk=~clk;
      data='h41;
      #10 send=1;
   end

   initial #10000 $finish;
   
   top uuv (.*);

endmodule // top_tb

       