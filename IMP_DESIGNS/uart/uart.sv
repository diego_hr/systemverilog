module uart ( output logic tx,
   			  output logic rdy,
   			  output logic busy,
   			  input logic [7:0] data,
   			  input logic clk,
   			  input logic reset,
	                  input logic send
   			 );
	  
enum {idle, start, d0, d1, d2, d3, d4, d5, d6, d7, parity, stop} present, next;
	  
always_ff @( posedge clk, posedge reset )
	begin
		if( reset )
			present<=idle;
		else
			present<=next;
			
	   case( present )
	     idle: begin tx<='1; rdy<='1; busy<='0; end
	     start: begin tx<='0; rdy<='0; busy<='1; end
	     
	     d0: begin tx<=data[0]; rdy<='0; busy<='1; end
             d1: begin tx<=data[1]; rdy<='0; busy<='1; end
	     d2: begin tx<=data[2]; rdy<='0; busy<='1; end
	     d3: begin tx<=data[3]; rdy<='0; busy<='1; end
             d4: begin tx<=data[4]; rdy<='0; busy<='1; end
	     d5: begin tx<=data[5]; rdy<='0; busy<='1; end
             d6: begin tx<=data[6]; rdy<='0; busy<='1; end
	     d7: begin tx<=data[7]; rdy<='0; busy<='1; end
	     parity: begin tx<='1; rdy<='0; busy<='1; end
	     stop: begin tx<='1; rdy<='0; busy<='1; end
	     default: present<=idle;
	   endcase // case ( state )
	end
	
always_comb
	begin
		case( present )
			
          idle: if( send) next=start;
		        else next=idle;
		  
          start: next=d0;
		  
		  d0: next=d1;
		  d1: next=d2;
		  d2: next=d3;
		  d3: next=d4;
		  d4: next=d5;
		  d5: next=d6;
		  d6: next=d7;
		  d7: next=parity;
		  
		  parity: next=stop;
		  stop: next=idle;
		  
		  default: next=idle;
		endcase
	end
endmodule
   