module baud_gen_tb;
   reg clk;
   reg reset;
   wire baud_clk;
   logic probea;

   initial begin
      #0 clk=1;
      #0 reset=1;
      #10 reset=1;
      #10 reset=0;
      if ( reset==0 ) $display( "reset task occured" );
   end 
   
   always begin
      #10 clk=!clk;
      probea=probea+1;
      if( probea==27 ) begin 
        $display( "the baud clk now has bee to tick and have the value %b", baud_clk);
	 probea=0;
      end
   end

   baud_gen UUT (.clk(clk), .reset(reset), .baud_clk(baud_clk));
endmodule // baud_gen_tb
