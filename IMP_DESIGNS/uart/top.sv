`timescale 1ns/1ps

module top( input logic clk,
	    input logic reset,
	    input logic send,
	    input logic [7:0] data,
	    output logic rdy,
	    output logic busy,
	    output logic tx
	    );

   logic 		 g_clk;

   baud_gen u0(.*, .baud_clk(g_clk));
   uart u1(.*, .clk(g_clk));

endmodule // top
