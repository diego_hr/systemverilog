

End Points:

   Index   Instance          Pin   Type   Start Clock      End Clock        Clock Delay   Req Per    Slack
   -----   --------          ---   ----   -----------      ---------        -----------   -------    -----
       1   baud_clk          D     reg    baud_gen|clk:r   baud_gen|clk:r         4.406     1.691   -0.224
       2   tickgen[6]        D     reg    baud_gen|clk:r   baud_gen|clk:r         4.406     1.691   -0.224
       3   tickgen[1]        D     reg    baud_gen|clk:r   baud_gen|clk:r         4.406     1.691   -0.216
       4   tickgen[3]        D     reg    baud_gen|clk:r   baud_gen|clk:r         4.406     1.691   -0.216
       5   tickgen[4]        D     reg    baud_gen|clk:r   baud_gen|clk:r         4.406     1.691   -0.216
       6   tickgen[5]        D     reg    baud_gen|clk:r   baud_gen|clk:r         4.406     1.691   -0.116
       7   tickgen[2]        D     reg    baud_gen|clk:r   baud_gen|clk:r         4.406     1.691    0.076
       8   tickgen_fast[0]   D     reg    baud_gen|clk:r   baud_gen|clk:r         4.406     1.691    0.293
       9   tickgen[0]        D     reg    baud_gen|clk:r   baud_gen|clk:r         4.406     1.691    0.302


Start Points:

   Index   Instance          Pin   Type   Start Clock      Clock Delay    Slack
   -----   --------          ---   ----   -----------      -----------    -----
       1   tickgen[1]        Q     reg    baud_gen|clk:r         4.406   -0.224
       2   tickgen[1]        Q     reg    baud_gen|clk:r         4.406   -0.224
       3   tickgen[1]        Q     reg    baud_gen|clk:r         4.406   -0.216
       4   tickgen[1]        Q     reg    baud_gen|clk:r         4.406   -0.216
       5   tickgen[2]        Q     reg    baud_gen|clk:r         4.406   -0.216
       6   tickgen[1]        Q     reg    baud_gen|clk:r         4.406   -0.116
       7   tickgen[1]        Q     reg    baud_gen|clk:r         4.406    0.076
       8   tickgen_fast[0]   Q     reg    baud_gen|clk:r         4.406    0.293
       9   tickgen[0]        Q     reg    baud_gen|clk:r         4.406    0.302
