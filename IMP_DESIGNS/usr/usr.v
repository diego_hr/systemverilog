`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04/16/2014 10:38:01 AM
// Design Name: 
// Module Name: usr
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module usr #(parameter N=8) 
            ( input clk, reset, 
              input [1:0] control,
              output [N-1:0] d,
              output [N-1:0] q );
              
reg [N-1:0] r_reg, r_next;

    always @( posedge clk, posedge reset )
    begin
        if( reset )
            r_reg <= 0;
        else
            r_reg <= r_next;
    end
    
    always @( * )
    begin
        case( control )
            2'b00: r_next = r_reg; // No operation
            2'b01: r_next = { r_reg [N-2:0], d[0] }; // Shift left
            2'b10: r_next = { d[N-1], r_reg[N-1:1] }; //Shift right
            2'b11: r_next = d;
            default: r_next = d;    //Load
        endcase
    end

assign q = r_reg;
 
endmodule