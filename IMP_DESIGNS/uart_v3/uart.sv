`timescale 1ns/1ps

module uart( input logic clk, reset, sendb, [7:0] data,
	     output logic rdy, bsy, tx );

   typedef  enum 	  logic [3:0] {idle, start, d0, d1, d2, d3, d4, d5, d6, d7, parity, stop} pstate;
   pstate state, nextstate;
   

   always_ff @( posedge clk, posedge reset )
     begin
     	if( reset )
     		begin
     			state<=idle;
     		end
     	else
     		state<=nextstate;
     end

   always_comb
     begin
	nextstate=state;
	unique case( state )
	  idle: begin if( sendb ) begin nextstate=start; tx='1; rdy='1; bsy='0; end else begin nextstate=idle; tx='1; rdy='1; bsy='0; end end
	  start: begin tx='0; rdy='0; bsy='1; nextstate=d0; end
	  
	  d0: begin tx=data[0]; rdy='0; bsy=1; nextstate=d1; end
	  d1: begin tx=data[1]; rdy='0; bsy=1; nextstate=d2; end
	  d2: begin tx=data[2]; rdy='0; bsy=1; nextstate=d3; end
	  d3: begin tx=data[3]; rdy='0; bsy=1; nextstate=d4; end
	  d4: begin tx=data[4]; rdy='0; bsy=1; nextstate=d5; end
	  d5: begin tx=data[5]; rdy='0; bsy=1; nextstate=d6; end
	  d6: begin tx=data[6]; rdy='0; bsy=1; nextstate=d7; end
	  d7: begin tx=data[7]; rdy='0; bsy=1; nextstate=parity; end
	 
	  parity: begin tx='1; rdy='0; bsy='1; nextstate=stop; end
	  stop: begin tx='1; rdy='1; bsy='1; nextstate=idle; end
	  
	endcase // case ( state )
     end
endmodule // uart