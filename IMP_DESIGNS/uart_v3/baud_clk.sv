`timescale 1ns/1ps

module baud_clk( input logic clk, reset,
		 output logic bclk );

   integer sclk;

   always_ff @( posedge clk, posedge reset )
     begin
	if( reset )
	  begin
	     sclk<='0;
	     bclk<='0;
	  end
	else if( sclk==14 )
	  begin
	     bclk<=~bclk;
	     sclk<='0;
	  end
	else sclk<=sclk+1;
     end // always_ff @ ( posedge busin.clk, posedge busin.reset )
endmodule // baud_clk

	     