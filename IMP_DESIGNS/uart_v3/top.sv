`timescale 1ns/1ps

module top( input logic clk, reset, sendb, [7:0] data,
	    output logic tx, rdy, bsy );

   logic 		 bclk;

   baud_clk baud(.*);
   uart transm(.*, .clk(bclk));
endmodule // top