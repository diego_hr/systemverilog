`timescale 1ns/1ps

module piso #( parameter N=8 ) ( input logic clk, reset, load, 
				 input logic [N-1:0] din,
				 output logic dout );

   logic [N-1:0] 			      temp;

   always_ff @(posedge clk, posedge reset, posedge load)
     begin
	if ( reset )
	  temp<='0;
	else if( load )
	  temp<=din;
	else
	  temp<={1'b0, temp[N-1:1]};
     end

   always_comb
     begin
	dout=temp[0];
     end
endmodule // piso