
# 1000 : define_clock [get_ports {clk}] -name {top|clk} -ref_rise {0.0} -ref_fall {0.886450} -uncertainty {0.000000} -period {1.772900} -clockgroup {Autoconstr_clkgroup_0} -rise {0.0} -fall {0.886450} 

create_clock  -name {top|clk} [get_ports {clk}]  -period {   1.773} -waveform {0.000 0.886}


# 1001 : define_clock [get_nets {baud.bclk}] -name {baud_clk|bclk_derived_clock} -ref_rise {0.0} -ref_fall {0.886450} -uncertainty {0.000000} -period {1.772900} -clockgroup {Autoconstr_clkgroup_0} -rise {0.0} -fall {0.886450} 



#Constraints which are not forward annotated in XDC and intentionally commented out (unused and unsupported constraints)

#Synplify Premier Physical Plus Generated Physical Constraints
#User specified region constraints
# User specified SLR assignment for objects.
create_pblock slr0
create_pblock slr1
create_pblock slr2
create_pblock slr3
resize_pblock [get_pblocks slr0] -add {CLOCKREGION_X0Y0:CLOCKREGION_X1Y2}
resize_pblock [get_pblocks slr1] -add {CLOCKREGION_X0Y3:CLOCKREGION_X1Y5}
resize_pblock [get_pblocks slr2] -add {CLOCKREGION_X0Y6:CLOCKREGION_X1Y8}
resize_pblock [get_pblocks slr3] -add {CLOCKREGION_X0Y9:CLOCKREGION_X1Y11}
