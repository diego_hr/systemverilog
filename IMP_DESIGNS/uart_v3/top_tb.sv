`timescale 1ns/1ps

module top_tb;
   logic clk;
   logic reset;
   logic [7:0] data;
   logic       sendb;
   logic       rdy;
   logic       bsy;
   logic       tx;


   initial begin
      #0 clk=0;
      #0 sendb=0;
      #10 reset=1;
      #10 reset=0;
      #100 reset=1;
      #100 reset=0;
      $display( "reset is %b", reset );
   end

   always begin
      #10 clk=~clk;
      #0 data='h41;
      #100 sendb=1;
      $display( "reset=%b  data=%h  send=%b  rdy=%b  bsy=%b  tx=%b", reset, data, sendb, rdy, bsy, tx );
   end

   initial #80000 $finish;

   top uuv (.*);
  // uart uuv(.*);
endmodule // top_tb
