package defs;

   typedef logic [7:0] uint_8;
   typedef logic [3:0] uint_4;
   typedef int         uint_32;

endpackage // defs
   