`timescale 1ns/1ps
//`include "defs.sv"
`include "mainbus.sv"

import defs::*;

module state_machine ( mainbus.bout bus );

enum logic [2:0] { start, d0, d1, d2, d3, stop } state, n_state;

   always_ff @( posedge bus.iClk, posedge bus.iReset )
     begin
	if( bus.iReset )
	  state<=start;
	else
	  state<=n_state;
     end

   always_comb
     begin
	n_state=state;
	case( state )
	     start: begin
		bus.oData=5'b00000;
		n_state=d0;
	     end

	  d0: begin
	     bus.oData=5'b00001;
	     n_state=d1;
	  end

	  d1: begin
	     bus.oData=5'b00001;
	     n_state=d2;
	  end

	  d2: begin
	     bus.oData=5'b00011;
	     n_state=d3;
	  end

	  d3: begin
	     bus.oData=5'h00100;
	     n_state=stop;
	  end

	  stop: begin
	     bus.oData=5'b11111;
	     n_state=start;
	  end
	endcase // case ( state )
     end // always_comb begin
endmodule // state_machine
