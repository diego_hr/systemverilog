`include "defs.sv"

interface mainbus ( input logic iClk );
   logic iReset;
   logic [4:0] oData;

   modport bout( input iClk,
		 input iReset,
		 output oData );

 endinterface
