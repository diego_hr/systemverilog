
proc syn_dump_io {} {
	execute_module -tool cdb -args "--back_annotate=pin_device"
}

source "/home/diego/EDA/Synopsys/lib/altera/quartus_cons.tcl"
syn_create_and_open_prj fsm_tmp
source $::quartus(binpath)/prj_asd_import.tcl
syn_create_and_open_csf fsm_tmp
syn_handle_cons fsm_tmp
syn_dump_io
