`timescale 1ns/1ps

interface busc( input logic clk );
   logic clk;
   logic reset;
   logic bclk;

   modport busout( input clk, reset,
	       output bclk );

endinterface // bus

module baud_clk( busc.busout busout );

   integer sclk;

   always_ff @( posedge busout.clk, posedge busout.reset )
     begin
	if( busout.reset )
	  begin
	     sclk<='0;
	     busout.bclk<='0;
	  end
	else if( sclk==14 )
	  begin
	     busout.bclk<=~busout.bclk;
	     sclk<='0;
	  end
	else sclk<=sclk+1;
     end // always_ff @ ( posedge busin.clk, posedge busin.reset )
endmodule // baud_clk

	     