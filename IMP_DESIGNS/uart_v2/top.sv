`timescale 1ns/1ps

interface bustop( input logic clk );
   logic clk;
   logic reset;
   logic bclk;
   logic sendb;
   logic [7:0] data;
   logic rdy;
   logic bsy;
   logic tx;

   modport busout( input clk, reset, sendb, data,
	       output rdy, bsy, tx );

endinterface // bus

module top(bustop.busout busout);

   logic bclk;
   
   busc thebus(.clk(clk));
   baud_clk baudgen (.busout(bustop));
   


endmodule // top



   