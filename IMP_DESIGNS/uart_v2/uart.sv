`timescale 1ns/1ps

interface bus( input logic clk );
   logic reset;
   logic sendb;
   logic [7:0] data;
   logic rdy;
   logic bsy;
   logic tx;

   modport busout( input reset, clk, sendb, data,
		   output rdy, bsy, tx );
endinterface // bus

module uart( bus.busout busout );

  enum logic [6:0] {idle, start, d0, d1, d2, d3, d4, d5, d6, d7, parity, stop} state, nextstate;

   always_ff @( posedge busout.clk, posedge busout.reset )
     begin
     	if( busout.reset )
     		begin
     			state<=idle;
     		end
     	else
     		state<=nextstate;
     end

   always_comb
     begin
	nextstate=state;
	case( state )
	  idle: begin if( busout.sendb ) begin nextstate=start; busout.tx='1; busout.rdy='1; busout.bsy='0; end else begin nextstate=idle; busout.tx='1; busout.rdy='1; busout.bsy='0; end end
	  start: begin busout.tx='0; busout.rdy='0; busout.bsy='1; nextstate=d0; end
	  d0: begin busout.tx=busout.data[0]; busout.rdy='0; busout.bsy=1; nextstate=d1; end
	  d1: begin busout.tx=busout.data[1]; busout.rdy='0; busout.bsy=1; nextstate=d2; end
	  d2: begin busout.tx=busout.data[2]; busout.rdy='0; busout.bsy=1; nextstate=d3; end
	  d3: begin busout.tx=busout.data[3]; busout.rdy='0; busout.bsy=1; nextstate=d4; end
	  d4: begin busout.tx=busout.data[4]; busout.rdy='0; busout.bsy=1; nextstate=d5; end
	  d5: begin busout.tx=busout.data[5]; busout.rdy='0; busout.bsy=1; nextstate=d6; end
	  d6: begin busout.tx=busout.data[6]; busout.rdy='0; busout.bsy=1; nextstate=d7; end
	  d7: begin busout.tx=busout.data[7]; busout.rdy='0; busout.bsy=1; nextstate=parity; end
	  parity: begin busout.tx='1; busout.rdy='0; busout.bsy='1; nextstate=stop; end
	  stop: begin busout.tx='1; busout.rdy='1; busout.bsy='1; nextstate=idle; end
	endcase // case ( state )
     end
endmodule // uart