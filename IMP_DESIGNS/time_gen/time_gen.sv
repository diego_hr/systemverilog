module time_gen( input logic clk,
		 input logic reset,
		 output logic q );


   int 			      temp;

   always_ff @( posedge clk, negedge reset )
     begin
	if( ~reset )
	  begin
	     temp<='0;
	     q<='0;
	  end
	else if( temp == 50000000 )
	  begin
	     q <= ~q;
	     temp<='0;
	  end
	else
	  temp <= temp + '1;
     end // always_ff @ ( posedge clk, negedge reset )
endmodule // time_gen