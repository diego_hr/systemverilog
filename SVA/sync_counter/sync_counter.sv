module sync_counter ( output logic [7:0] q,
		      input logic clk,
		      input logic reset,
		      input logic en );


   logic [7:0] 			  temp;

   always_ff @( posedge clk )
     begin
	if( reset )
	  temp='0;
	else if ( en )
	  temp++;
	else
	  temp='z;
     end

   always_comb
     q=temp;
endmodule // sync_counter