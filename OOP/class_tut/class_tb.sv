module class_tb;
class C;
   int x;
   task set (int i);
      x=i;
   endtask // set
   function int get;
      return x;
   endfunction //
endclass // C

   C c1=new;

   initial begin
      c1.set(3);
      $display("c1.x is %d", c1.get());
   end
endmodule // class_tb
