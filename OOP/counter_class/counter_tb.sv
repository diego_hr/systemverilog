module counter_tb;
   logic clk, reset, u_d, en, stop;
   logic [7:0] q;


class tester_counter;
   task test_stop();
      stop=1;
   endtask // test_stop

   task test_upc();
      u_d=1;
   endtask // test_upc

   task test_doc();
      u_d=0;
   endtask // test_doc

   task test_en();
      en=1;
   endtask // test_en

   task reset_ports();
      reset=1;
      #10 reset=0;
   endtask // reset_ports

   task init_ports();
      reset=0;
      u_d=0;
      stop=0;
      en=0;
      clk=0;
   endtask // init_ports
   
endclass // tester_counter


   tester_counter test_0=new;

   initial begin
      #10 test_0.init_ports();
      #10 test_0.reset_ports();
      $display("ud=%b, en=%b, stop=%b",u_d, en, stop,);
   end

   always begin
      #10 clk=!clk;
      test_0.test_en();
      if(q == 0) $display("Test en passed!");
      test_0.test_upc();
      if(q==255) begin
	 $display(" test up counter passed!");
      end
      test_0.init_ports();
      #10 test_0.test_en();
      test_0.test_doc();
      if(q==0) begin
	 $display("Test down counter passed!");
      end
   end

   initial #300000 $finish;
   
   counter uud (.*);
      
endmodule // counter_tb

      