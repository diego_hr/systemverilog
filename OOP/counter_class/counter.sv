module counter ( input logic clk,
		 input logic reset,
		 input logic u_d,
		 input logic en,
		 input logic stop,
		 output logic [7:0] q );

   logic [7:0] 			    c;

   always_ff @( posedge clk, posedge reset )
     begin
	if( reset )
	  c='0;
	else if( en )
	  begin
	     if(u_d)
	       c++;
	     else if (~u_d)
	       c--;
	     if (stop)
	       c=c;
	     else;
	       //c='z;
	  end
     end

   always_comb
     begin
	q=c;
     end
endmodule // counter