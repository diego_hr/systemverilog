module top_tb;
   logic clk, reset, u_d, load;
   logic [7:0] ldata, q;


class test;

   task reset_port();
	#10 reset=1;
	#10 reset=0;
   endtask // reset

   task clear_ports();
      u_d=0;
      load=0;
      ldata=0;
      clk=0;
   endtask // clear_ports

   task up_down( logic val );
      u_d=val;
   endtask // up_down

   task check_load( logic [7:0] data_for_load );
      ldata=data_for_load;
      load=1;
   endtask // check_load

   function void count_up;
      this.up_down( 1'b1 );
      $display (" Q = %b", q );
      if(q==255) begin
	 $display( "[OK] UP COUNTER TO MAX VALUE" );
	 $stop;
      end
   endfunction // up_down

   function void count_down;
      this.up_down( 1'b0 );
      $display ( "Q = %b", q);
      if( q == 1 ) begin
	 $display( "[OK] DOWN COUNTER TO MIN VALUE" );
	 $stop;
      end
   endfunction // up_down
   
endclass // test

   test my_test=new;

   initial begin
      #10 my_test.clear_ports();
      #10 my_test.reset_port();
   end
   
   always begin
      #10 clk=~clk;
      my_test.count_up;
     // my_test.count_down;
      end
   end // always begin

   //initial begin #100000 $finish; end
   
   top uud (.*);

endmodule // top

      
      