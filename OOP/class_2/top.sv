module top ( input logic clk,
	    input logic reset,
	    input logic u_d,
	    input logic load,
	    input logic [7:0] ldata,
	    output logic [7:0] q );

   logic [7:0] 		       temp;


   always_ff @( posedge clk, posedge reset )
     begin
	if( reset )
	  temp='0;
	else if ( load )
	  temp=ldata;
	  else if  ( u_d )
	    temp++;
	  else
	    temp--;
     end // always_ff @ ( posedge clk, posedge reset )

   assign q=temp;
   
endmodule // top
