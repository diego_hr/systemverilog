`timescale 1ns/1ps

module timegen ( input logic clk,
		 input logic  reset,
		 output logic gticks );


   logic [31:0] 	      tempc;


   always_ff @( posedge clk, posedge reset )
     begin
	if( reset )
	  begin
	     tempc='0;
	     gticks<='0;
	  end
	else if( tempc == 32'd15 )
	  begin
	     tempc=32'b1;
	     gticks<=~gticks;
	  end
	else
	  tempc++;
     end // always_ff @

endmodule // timegen
