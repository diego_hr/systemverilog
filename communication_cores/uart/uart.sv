`timescale 1ns/1ps

module uart( input logic txclk,
	     input logic       reset,
	     input logic       ld_tx_data,
	     input logic       tx_enable,
	     input logic [7:0] tx_data,
	     output logic      tx_out,
	     output logic      tx_empty
	   );


   reg [7:0]    tx_reg;
   reg          tx_over_run;
   reg [3:0] 	tx_cnt;
   
   always_ff @( posedge txclk, posedge reset )
     begin
	if( reset ) begin
	   tx_reg <= '0;
	   tx_empty<='1;
	   tx_over_run<='0;
	   tx_out<='1;
	   tx_cnt<='0;
	end
	else begin
	   if( ld_tx_data) begin
	      if( !tx_empty) begin
		 tx_over_run<='0;
	      end
	      else begin
		 tx_reg<=tx_data;
		 tx_empty<='0;
	      end
	   end
	   if( tx_enable && !tx_empty ) begin
	      tx_cnt <= tx_cnt+1;
	      if( tx_cnt==0 ) begin
		 tx_out<='0;
	      end
	      if( tx_cnt > 0 && tx_cnt < 9 ) begin
		 tx_out<=tx_reg[tx_cnt - 1];
	      end
	      if( tx_cnt == 9 )begin
		 tx_out <='1;
		 tx_cnt <='0;
		 tx_empty<='1;
	      end
	   end // if ( tx_enable && !tx_empty )
	   if(!tx_enable) begin
	      tx_cnt <='0;
	   end
	end // else: !if( reset )
     end // always_ff @
   // 	
endmodule
		 
	   
