`timescale 1ns/1ps
`include "gdefs.sv"

import gdefs::*; //Import all from gdefs package

module top( input  uintb clk,
	    input  uintb reset,
	    input  uintb en,
	    input  uintb dir,
	    output uint8 q );

//Declare some internal registers here
   uint8 tempq; //remeber uint8 is a 8 bit logic var
   
//Secuential logic
   always_ff @( posedge clk, negedge reset )
     begin
	if( ~reset )
	  tempq='0;
	else begin
	  if( en )
	     if( dir )
	       tempq++;
	     else
	       tempq--;
	end
     end // always_ff @

//Combinational logic
   assign q=tempq;

endmodule // top

	
