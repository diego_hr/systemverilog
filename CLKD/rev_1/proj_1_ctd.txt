

End Points:

   Index   Instance          Pin     Type   Start Clock   End Clock    Clock Delay   Req Per    Slack
   -----   --------          ---     ----   -----------   ---------    -----------   -------    -----
       1   count_value[12]   sload   reg    clkd|clk:r    clkd|clk:r         0.000    20.000   15.454
       2   count_value[13]   sload   reg    clkd|clk:r    clkd|clk:r         0.000    20.000   15.454
       3   count_value[14]   sload   reg    clkd|clk:r    clkd|clk:r         0.000    20.000   15.454
       4   count_value[15]   sload   reg    clkd|clk:r    clkd|clk:r         0.000    20.000   15.454
       5   count_value[17]   sload   reg    clkd|clk:r    clkd|clk:r         0.000    20.000   15.454
       6   count_value[19]   sload   reg    clkd|clk:r    clkd|clk:r         0.000    20.000   15.454
       7   count_value[20]   sload   reg    clkd|clk:r    clkd|clk:r         0.000    20.000   15.454
       8   count_value[21]   sload   reg    clkd|clk:r    clkd|clk:r         0.000    20.000   15.454
       9   count_value[22]   sload   reg    clkd|clk:r    clkd|clk:r         0.000    20.000   15.454
      10   count_value[23]   sload   reg    clkd|clk:r    clkd|clk:r         0.000    20.000   15.454
      11   count_value[25]   sload   reg    clkd|clk:r    clkd|clk:r         0.000    20.000   15.454
      12   count_value[2]    sload   reg    clkd|clk:r    clkd|clk:r         0.000    20.000   15.454
      13   count_value[3]    sload   reg    clkd|clk:r    clkd|clk:r         0.000    20.000   15.454
      14   count_value[7]    d       reg    clkd|clk:r    clkd|clk:r         0.000    20.000   15.623
      15   count_value[8]    d       reg    clkd|clk:r    clkd|clk:r         0.000    20.000   15.623
      16   count_value[30]   d       reg    clkd|clk:r    clkd|clk:r         0.000    20.000   17.909
      17   count_value[31]   d       reg    clkd|clk:r    clkd|clk:r         0.000    20.000   17.909
      18   count_value[28]   d       reg    clkd|clk:r    clkd|clk:r         0.000    20.000   17.975
      19   count_value[29]   d       reg    clkd|clk:r    clkd|clk:r         0.000    20.000   17.975
      20   count_value[26]   d       reg    clkd|clk:r    clkd|clk:r         0.000    20.000   18.041
      21   count_value[27]   d       reg    clkd|clk:r    clkd|clk:r         0.000    20.000   18.041
      22   count_value[24]   d       reg    clkd|clk:r    clkd|clk:r         0.000    20.000   18.107
      23   count_value[25]   d       reg    clkd|clk:r    clkd|clk:r         0.000    20.000   18.107
      24   count_value[22]   d       reg    clkd|clk:r    clkd|clk:r         0.000    20.000   18.173
      25   count_value[23]   d       reg    clkd|clk:r    clkd|clk:r         0.000    20.000   18.173
      26   count_value[20]   d       reg    clkd|clk:r    clkd|clk:r         0.000    20.000   18.239
      27   count_value[21]   d       reg    clkd|clk:r    clkd|clk:r         0.000    20.000   18.239
      28   count_value[18]   d       reg    clkd|clk:r    clkd|clk:r         0.000    20.000   18.305
      29   count_value[19]   d       reg    clkd|clk:r    clkd|clk:r         0.000    20.000   18.305
      30   count_value[16]   d       reg    clkd|clk:r    clkd|clk:r         0.000    20.000   18.371
      31   count_value[17]   d       reg    clkd|clk:r    clkd|clk:r         0.000    20.000   18.371
      32   count_value[14]   d       reg    clkd|clk:r    clkd|clk:r         0.000    20.000   18.437
      33   count_value[15]   d       reg    clkd|clk:r    clkd|clk:r         0.000    20.000   18.437
      34   count_value[12]   d       reg    clkd|clk:r    clkd|clk:r         0.000    20.000   18.503
      35   count_value[13]   d       reg    clkd|clk:r    clkd|clk:r         0.000    20.000   18.503
      36   count_value[10]   d       reg    clkd|clk:r    clkd|clk:r         0.000    20.000   18.569
      37   count_value[11]   d       reg    clkd|clk:r    clkd|clk:r         0.000    20.000   18.569
      38   count_value[9]    d       reg    clkd|clk:r    clkd|clk:r         0.000    20.000   18.635
      39   count_value[6]    d       reg    clkd|clk:r    clkd|clk:r         0.000    20.000   18.701
      40   count_value[4]    d       reg    clkd|clk:r    clkd|clk:r         0.000    20.000   18.767
      41   count_value[5]    d       reg    clkd|clk:r    clkd|clk:r         0.000    20.000   18.767
      42   count_value[2]    d       reg    clkd|clk:r    clkd|clk:r         0.000    20.000   18.833
      43   count_value[3]    d       reg    clkd|clk:r    clkd|clk:r         0.000    20.000   18.833
      44   count_value[1]    d       reg    clkd|clk:r    clkd|clk:r         0.000    20.000   18.897
      45   count_value[0]    d       reg    clkd|clk:r    clkd|clk:r         0.000    20.000   19.334


Start Points:

   Index   Instance          Pin   Type   Start Clock   Clock Delay    Slack
   -----   --------          ---   ----   -----------   -----------    -----
       1   count_value[22]   q     reg    clkd|clk:r          0.000   15.454
       2   count_value[22]   q     reg    clkd|clk:r          0.000   15.454
       3   count_value[22]   q     reg    clkd|clk:r          0.000   15.454
       4   count_value[22]   q     reg    clkd|clk:r          0.000   15.454
       5   count_value[22]   q     reg    clkd|clk:r          0.000   15.454
       6   count_value[22]   q     reg    clkd|clk:r          0.000   15.454
       7   count_value[22]   q     reg    clkd|clk:r          0.000   15.454
       8   count_value[22]   q     reg    clkd|clk:r          0.000   15.454
       9   count_value[22]   q     reg    clkd|clk:r          0.000   15.454
      10   count_value[22]   q     reg    clkd|clk:r          0.000   15.454
      11   count_value[22]   q     reg    clkd|clk:r          0.000   15.454
      12   count_value[22]   q     reg    clkd|clk:r          0.000   15.454
      13   count_value[22]   q     reg    clkd|clk:r          0.000   15.454
      14   count_value[22]   q     reg    clkd|clk:r          0.000   15.623
      15   count_value[22]   q     reg    clkd|clk:r          0.000   15.623
      16   count_value[1]    q     reg    clkd|clk:r          0.000   17.909
      17   count_value[1]    q     reg    clkd|clk:r          0.000   17.909
      18   count_value[1]    q     reg    clkd|clk:r          0.000   17.975
      19   count_value[1]    q     reg    clkd|clk:r          0.000   17.975
      20   count_value[1]    q     reg    clkd|clk:r          0.000   18.041
      21   count_value[1]    q     reg    clkd|clk:r          0.000   18.041
      22   count_value[1]    q     reg    clkd|clk:r          0.000   18.107
      23   count_value[1]    q     reg    clkd|clk:r          0.000   18.107
      24   count_value[1]    q     reg    clkd|clk:r          0.000   18.173
      25   count_value[1]    q     reg    clkd|clk:r          0.000   18.173
      26   count_value[1]    q     reg    clkd|clk:r          0.000   18.239
      27   count_value[1]    q     reg    clkd|clk:r          0.000   18.239
      28   count_value[1]    q     reg    clkd|clk:r          0.000   18.305
      29   count_value[1]    q     reg    clkd|clk:r          0.000   18.305
      30   count_value[1]    q     reg    clkd|clk:r          0.000   18.371
      31   count_value[1]    q     reg    clkd|clk:r          0.000   18.371
      32   count_value[1]    q     reg    clkd|clk:r          0.000   18.437
      33   count_value[1]    q     reg    clkd|clk:r          0.000   18.437
      34   count_value[1]    q     reg    clkd|clk:r          0.000   18.503
      35   count_value[1]    q     reg    clkd|clk:r          0.000   18.503
      36   count_value[1]    q     reg    clkd|clk:r          0.000   18.569
      37   count_value[1]    q     reg    clkd|clk:r          0.000   18.569
      38   count_value[1]    q     reg    clkd|clk:r          0.000   18.635
      39   count_value[1]    q     reg    clkd|clk:r          0.000   18.701
      40   count_value[1]    q     reg    clkd|clk:r          0.000   18.767
      41   count_value[1]    q     reg    clkd|clk:r          0.000   18.767
      42   count_value[1]    q     reg    clkd|clk:r          0.000   18.833
      43   count_value[1]    q     reg    clkd|clk:r          0.000   18.833
      44   count_value[1]    q     reg    clkd|clk:r          0.000   18.897
      45   count_value[0]    q     reg    clkd|clk:r          0.000   19.334
