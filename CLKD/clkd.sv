`timescale 1ns/1ps

module clkd #( parameter clkf_in=50000000, 
	       clkf_out=500 )
             ( input logic clk,
	       input logic reset,
	       output logic gclk );

   integer 		    count_value;
   
   
   always_ff @( posedge clk, posedge reset )
     begin
	if( reset )
	  count_value<=0;
	else begin
	   count_value<=clkf_in;
	   if( count_value==clkf_out)
	     begin
		count_value<=clkf_in-1;
	     end
	   else
	     count_value<=count_value-1;
	end // else: !if( reset )
     end // always_ff @ ( posedge clk )

   always_comb
     begin
	gclk=(count_value==0) ? 1'b1 : 1'b0;
     end

endmodule // clkd


	
	     
   