`timescale 1ns/1ps

module clkd_tb;

   logic clk;
   logic reset;
   logic gclk;


   initial begin
      #10 clk=0;
      #10 reset=0;
      #10 reset=1;
      #10 reset=0;
   end

   always begin
      #10 clk=~clk;
   end

   clkd #(.clkf_in(10), .clkf_out(2)) uud (.*);

endmodule // clkd_tb
