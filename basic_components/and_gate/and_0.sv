//////////////////////////////////////////////////////////
//                                                      //
//      Simple AND gate with two inputs, one output.    //
// Created by: Diego HR.                                //
// Version: 1.0                                         //
// Synth tool: Synplify Premiere                        //
//////////////////////////////////////////////////////////
`timescale 1ns/1ps

module and_0( input logic a, b,
	      output logic c );


   assign c=a & b; //some magic here

endmodule
   
