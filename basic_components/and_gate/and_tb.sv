//////////////////////////////////////////////////////////
//                                                      //
//      Simple AND gate with two inputs, one output.    //
// Created by: Diego HR.                                //
// Version: 1.0                                         //
// Synth tool: VCS mx                                   //
//////////////////////////////////////////////////////////
`timescale 1ns/1ps

module and_tb;
   logic a, b;
   logic c;


   initial begin
      #5 a=0;
      #5 b=0;
   end

   always begin
      #5 a=0;
      #5 b=0;

      #5 a=1;
      #5 b=0;

      #5 a=0;
      #5 b=1;

      #5 a=1;
      #5 b=1;

      $display( "@Time=%t, a=%b, b=%b c=%b\n", $time, a, b, c);
   end // always begin

   initial #1000 $finish;

     and_0 uud ( .* );
endmodule // and_tb
